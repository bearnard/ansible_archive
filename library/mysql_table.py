#!/usr/bin/python

DOCUMENTATION = '''
---
module: mysql_table
version_added: "2.1"
short_description: Archive your mysql tables
options:
  mode:
    description:
      - The mode to be used dump or drop
    required: true
    default: dump
  host:
    description:
      - The database host
    required: true
  db_user:
    description:
      - The db user
    required: true
  db_passwd:
    description:
      - The db password
    required: true
  db_name:
    description:
      - The db name
    required: true
  prefix:
    description:
      - The table prefix
    required: true
author:
    - "Bearnard Hibbins (@bearnard)"
'''

EXAMPLES = '''
- name: Create a archive table backup
  mysql_table:
    mode: dump
    host: "{{rdsdata.instance.endpoint}}"
    db_user: "{{db_user}}"
    db_passwd: "{{db_passwd}}"
    db_name: "{{database}}"
    prefix: "ARCHIVE_"
  register: result
- name: Drop archived tables
  mysql_table:
    mode: drop
    host: "{{rdsdata.instance.endpoint}}"
    db_user: "{{db_user}}"
    db_passwd: "{{db_passwd}}"
    db_name: "{{database}}"
    prefix: "ARCHIVE_"
  register: result
'''

RETURN = '''
meta:
    description: Dict conainting properties and results
    returned: always
    type: dict
    sample:
        archive: "/tmp/test-db.jhgjhgjhgv4.us-east-1.rds.amazonaws.com_testing_201609300549.sql.tar.gz"
        exit_code: 0
        key: test-db.jhgjhgjhgv4.us-east-1.rds.amazonaws.com_testing_201609300549.sql.tar.gz
        stderr: |
          ARCHIVE_testtable1
          ARCHIVE_testtable2
        stdout: ''
        tables:
        - ARCHIVE_testtable1
        - ARCHIVE_testtable2
'''


import os
import subprocess
import tarfile

from ansible.module_utils.basic import *

from datetime import datetime


def mysql_table_dump(data):

    tables, return_code, stderr, stdout = get_mysql_tables(data)

    meta = {}
    meta['exit_code'] = return_code
    meta['stderr'] = stderr
    meta['stdout'] = stdout
    meta['archive'] = ''

    if not tables:
        return False, False, meta

    if return_code > 0:
        return True, False, meta

    meta['tables'] = tables

    has_changed = len(tables) > 0
    archive = mysql_backup(data, tables, '/tmp')

    meta['archive'] = archive
    meta['key'] = os.path.basename(archive)
    return not archive, has_changed, meta


def get_mysql_tables(data):
    db_user = data['db_user']
    db_passwd = data['db_passwd']
    db_host = data['host']
    db_name = data['db_name']
    prefix = data['prefix']

    cmd = [
        'mysql',
        db_name,
        '-h' + db_host,
        '-u' + db_user,
        '-p' + db_passwd,
        '--batch', '--disable-column-names',
        '-e', 'show tables like "{0}%"'.format(prefix)]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    tables = []
    if stdout:
        tables = stdout.strip().split('\n')
    return tables, p.returncode, stdout, stderr


def mysql_table_drop(data):
    db_user = data['db_user']
    db_passwd = data['db_passwd']
    db_host = data['host']
    db_name = data['db_name']
    tables, return_code, stderr, stdout = get_mysql_tables(data)

    meta = {}
    meta['exit_code'] = return_code
    meta['stderr'] = stderr
    meta['stdout'] = stdout

    if not tables:
        return False, False, meta

    if return_code > 0:
        return True, False, meta

    meta = {
        'exit_code': None,
        'stdout': '',
        'stderr': '',
        'tables': [],
    }

    cmd = [
        'mysql',
        db_name,
        '-h' + db_host,
        '-u' + db_user,
        '-p' + db_passwd,
        '-e', 'DROP TABLE IF EXISTS {0}'.format(', '.join(tables))]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()

    meta['exit_code'] = p.returncode

    meta['stderr'] = stderr
    meta['stdout'] = stdout
    meta['cmd'] = cmd

    if p.returncode > 0:
        return True, False, meta

    return False, True, meta


def mysql_backup(data, tables, dest_dir):

    db_user = data['db_user']
    db_passwd = data['db_passwd']
    db_host = data['host']
    db_name = data['db_name']

    date_stamp = datetime.now().strftime('%Y%m%d%H%M')
    sql_file = db_host + '_' + db_name + '_' + date_stamp + '.sql'
    dumpfile = open(os.path.join(dest_dir, sql_file), 'w')
    cmd = [
        'mysqldump',
        db_name,
        '-h' + db_host,
        '-u' + db_user,
        '-p' + db_passwd,
    ]
    cmd.extend(tables)
    p = subprocess.Popen(cmd, stdout=dumpfile)
    retcode = p.wait()
    dumpfile.close()
    if retcode > 0:
        return False
    archive = compress(dest_dir, sql_file)
    os.remove(os.path.join(dest_dir, sql_file))
    return archive


def compress(dest_dir, sql_file):
    archive = os.path.join(dest_dir, sql_file) + '.tar.gz'
    with tarfile.open(archive, 'w:gz') as tar:
        tar.add(os.path.join(dest_dir, sql_file), arcname=sql_file)
    return archive


def main():

    fields = {
        "host": {"required": True, "type": "str"},
        "db_name": {"required": True, "type": "str"},
        "db_user": {"required": True, "type": "str"},
        "db_passwd": {"required": True, "type": "str"},
        "prefix": {"required": True, "type": "str"},
        "mode": {
            "default": "dump",
            "choices": ['dump', 'drop'],
            "type": 'str'
        },
    }

    actions = {
        "dump": mysql_table_dump,
        "drop": mysql_table_drop,
    }

    module = AnsibleModule(argument_spec=fields)
    is_error, has_changed, result = actions.get(
        module.params['mode'])(module.params)

    if not is_error:
        module.exit_json(changed=has_changed, meta=result)
    else:
        module.fail_json(msg="Error running mysql_table", meta=result)


if __name__ == '__main__':
    main()
