# Ansible Archiving Old Tables in Database Project

## Overview:

The goal of this project is to write an Ansible script that checks for the archived RDS MySQL
tables, backs them up to an AWS S3 bucket, and sends an alert via AWS SNS service (which in
turn sends email) once done. The system should also send an alert via AWS SNS service if the
script fails at any point during the process. The details are described below:

* Write an Ansible script to check for archived RDS MySQL tables (the tables are prefixed
with ARCHIVE_)
* Back them up into an AWS S3 bucket.
* Delete the archived tables once done.
* Send an alert once the process is complete. Also send an alert if any errors are
encountered.
* Make sure your code is readable and can be run

## Project layout:

* The `archive.yml` ansible-playbook makes use of a custom module `mysql_table` for interacting with mysql tables.
```
├── README.md
├── archive.yml
├── library
│   ├── mysql_table.py # Module for dumping/dropping of old tables
└── vars.yml
```
* See DOCUMENTATION and EXAMPLES in `mysql_table.py` for additional info.


## Installation:
```shell
# Install ansible
virtualenv --no-site-packages env
. env/bin/activate
pip install ansible
pip install boto # Required by rds, sns and s3 ansible modules
```

## Pre-requisites:

* This playbook uses rds, sns and s3 ansible modules which require some environment variables to be exported
```shell
export EC2_REGION=YOUR_REGION
export AWS_ACCESS_KEY=YOUR_KEY
export AWS_SECRET_KEY=YOUR_SECRET_KEY
```
* Populate `vars.yml` with all the required values
```yaml

    cat vars.yml
    ---
    rds_instance: INSTANCE_ID_HERE
    db_user: DB_USER_HERE
    db_passwd: DB_PASSWORD_HERE
    database: DB_NAME_HERE
    s3_bucket: S3_BUCKET_HERE
    sns_topic: SNS_TOPIC_HERE # SNS topic name configured to email.
```

## Assumptions:

* `mysql` and `mysqldump` are installed where this playbook will run.
* You have an SNS topic name configured in AWS to email.
* All values in `vars.yml` have been configured correctly.
* The `rds` instance endpoint must be accessible from the host that runs the playbook.

## Running:


```
ansible-playbook archive.yml

```

## Results.

* If there were any tables that had the configured `prefix` they should have gotten backed up to your s3 bucket in a `.tar.gz` format with a key format of `RDS-INSTANCE-ID_DBNAME_TIMESTAMP.sql.tar.gz`.

* You should recieve a `NOTICE:` email to the configured address you setup with the SNS topic.

* Try testing the failure case, change the credentials (db_user/db_passwd) in `vars.yml` to incorrect ones, you should get an `ERROR:` email


## Possible Future Work:
* Add unit tests.
* Support multiple databases
